import socket


class Server:
    def __init__(self):
        self.IP_ADDRESS = '127.0.0.1'
        self.PORT_NUMBER = 6666
        self.users = []

        self.socket = socket.socket()
        self.socket.bind((self.IP_ADDRESS, self.PORT_NUMBER))

    def accept(self):
        self.socket.listen()
        while True:
            self.socket.accept()


server = Server()
server.accept()
