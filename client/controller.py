import socket


class Connector:
    def __init__(self):
        self.PORT_NUMBER = 6666
        self.IP_ADDRESS = '127.0.0.1'

        self.socket = socket.socket()

    def connect(self):
        self.socket.connect((self.IP_ADDRESS, self.PORT_NUMBER))


Connector().connect()
